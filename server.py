#!/usr/bin/env python3

# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, request, Response, render_template, send_from_directory, Markup
from mytimetable import timetable_scraper
from mytimetable import gen_calendar as calendar
from mytimetable.key_dates_getter import KeyDatesGetter
from mytimetable.determine_current_trimester import determine_current_trimester
import os
import datetime as dt
import pytz
import sys
import logging
from raven.contrib.flask import Sentry

server = Flask(__name__)
localtz = pytz.timezone('Pacific/Auckland')

if len(sys.argv) > 1:
    sentry = Sentry(server, dsn=sys.argv[1])
else:
    sentry = Sentry(server)

print("Loading key dates...")
key_dates_getter = KeyDatesGetter('dates.html')
key_dates = key_dates_getter.key_dates
print("Loaded.")


@server.route("/api")
def landing():
    """Return a page to show that the API is online."""
    return "Welcome to the my-timetable HTTP end-point <3"


@server.route("/api/generate", methods=["POST"])
def generate():
    """Return an ICS file based on HTML input from a POST request."""
    sa_html = request.form['sa_html']
    trimester_number = int(request.form['trimester_selection'])
    if sa_html == "":
        return render_template('index.html', trimester_number=trimester_number, error="Please put your timetable source in the box below.")
    try:
        trimester = timetable_scraper.scrape(sa_html, trimester_number)
        response = Response(calendar.make_calendar(trimester, key_dates, trimester_number))
    except Exception as e:
        logging.warning(e)
        sentry.captureException()

        return render_template(
            'index.html',
            trimester_number=trimester_number,
            error=Markup('We can\'t quite understand some of the information in your timetable. If you want to help get your timetable working please email <a href="mailto:nick@nick.geek.nz">nick@nick.geek.nz</a>.')
        )

    response.headers["Content-Disposition"] = "attachment; filename=myAllocator.ics"
    response.headers["Content-Type"] = "text/calendar"
    return response


@server.route("/")
def client():
    """Load the dynamic index.html from web/index.html."""

    # We don't support Trimester 3
    trimester = determine_current_trimester(key_dates, dt.date.today())
    if trimester == 3:
        trimester = 1

    return render_template('index.html', trimester_number=trimester)


@server.route("/<path:path>")
def static_files(path):
    """Load static resources from public/* when called from /*."""
    return send_from_directory(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'public'), path)


if __name__ == "__main__":
    server.run(host='127.0.0.1')
