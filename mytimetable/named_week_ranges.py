# Use `cal -wm currentyear` to view week numbers

named_week_ranges = {
    "Trimester 1": [(10, 16), (18, 23)],
    "Trimester 1 (first 6 weeks)": [(10, 16)],
    "Trimester 1 (last 6 weeks)":  [(18, 23)],
    "Trimester 1 (starts 2nd week)": [(11, 16), (18, 23)],

    "Trimester 2": [(28, 33), (36, 41)],
    "Trimester 2 (first 6 weeks)": [(28, 33)],
    "Trimester 2 (last 6 weeks)":  [(36, 41)],
    "Trimester 2 (starts 2nd week)": [(29, 33), (36, 41)],

    "Trimester 1 + 2": [(10, 16), (18, 23), (28, 33), (36, 41)]
}
