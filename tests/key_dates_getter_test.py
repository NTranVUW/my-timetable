# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# import responses
from datetime import date
# from mytimetable.key_dates_getter import KeyDatesGetter
from mytimetable.datatypes import KeyDates


class TestGetDates:

    result = KeyDates(closed_dates={
                      date(2017, 2, 6): 'Waitangi Day (University closed).',
                      date(2017, 4, 14): 'Good Friday (University closed).',
                      date(2017, 4, 17): 'Easter Monday (University closed).',
                      date(2017, 4, 18): 'University closed.',
                      date(2017, 6, 5): 'Queen’s Birthday (University closed).',
                      date(2017, 10, 23): 'Labour Day (University closed).',
                      date(2017, 12, 22): 'Christmas–New Year break begins. University closed (until 7 January 2018 inclusive, to be confirmed).Mid-trimester break begins (November–February courses).'},
                      trimester_ranges={1: (date(2017, 3, 6),
                                            date(2017, 7, 5)),
                                        2: (date(2017, 7, 17),
                                            date(2017, 11, 19)),
                                        3: (date(2017, 11, 20))})

    # @responses.activate
    # def test_fetch_dates(self):
    #     dates_body = open('tests/samples/keydates.html').read()
    #     responses.add(responses.GET,
    #                   'https://www.victoria.ac.nz/students/study/dates',
    #                   body=dates_body,
    #                   status=200)
    #     key_dates = KeyDatesGetter('download').key_dates
    #     assert key_dates == self.result

    # def test_load_dates(self):
    #     key_dates = KeyDatesGetter('tests/samples/keydates.html').key_dates
    #     assert key_dates == self.result
